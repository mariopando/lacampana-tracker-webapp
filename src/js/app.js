//Core
var core = new (require('./src/core.js'))();

/* Load modules */
var modules = [
  new (require('./src/ui.js'))(),
  new (require('./src/routing.js'))()
];

//set modules
core.setModules(modules);

//Instance core
core.ready();
