// URL and endpoint constants
const API_URL = 'http://ec2-52-67-62-61.sa-east-1.compute.amazonaws.com:4000';
const LOGIN_URL = API_URL + '/auth/';
const SIGNUP_URL = API_URL + '/users/';

export default {

    // User object will let us check authentication status
    user: {
        authenticated: false,
        token: null
    },

    // Send a request to the login URL and save the returned JWT
    login(context, creds, redirect) {
        context.$http.post(LOGIN_URL, creds).then((response) => {
            if(response.data.status){
                localStorage.setItem('token', response.data.token);
                this.user.name = response.data.payload.name;
                this.user.position = response.data.payload.position;
                this.user.authenticated = true;

                // Redirect to a specified route
                if(redirect) {
                    core.modules.ui.router.go(redirect)
                }
            }
            else{
                context.error = response.data.message;
            }
        }, (err) => {
            console.log(err);
            context.error = err;
        });
    },
    // To log out, we just need to remove the token
    logout() {
        localStorage.removeItem('token');
        this.user.authenticated = false;
        core.modules.ui.router.go('login');
    },

    checkAuth() {
        var jwt = localStorage.getItem('token');
        if(jwt) {
            this.user.authenticated = true;
        }
        else {
            this.user.authenticated = false;
            core.modules.ui.router.go('login');
        }
    },

    // The object to be passed as a header for authenticated requests
    getAuthHeader() {
        return {
            'Authorization': 'Bearer ' + localStorage.getItem('token')
        }
    }
}