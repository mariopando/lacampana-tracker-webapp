// URL and endpoint constants
const API_URL = 'http://ec2-52-67-62-61.sa-east-1.compute.amazonaws.com:4000';
//const API_URL = 'http://localhost:4000';
const EVENTS_URL = API_URL + '/events?token='+localStorage.getItem('token');
const DISPLAY_URL = API_URL + '/displays?token='+localStorage.getItem('token');

export default {
    //user: Auth.user,
    getEvents(context,callback){
        context.$http.get(EVENTS_URL).then((response) => {
            if(response.data.status){
                //console.log(response.data.payload)
                callback(response.data.payload);
            }
            else{
                context.error = response.data.message;
            }
        }, (err) => {
            console.log(err)
            context.error = err
        });
    },
    getDisplays(context,callback){
        context.$http.get(DISPLAY_URL).then((response) => {
            if(response.data.status){
                //console.log(response.data.payload)
                callback(response.data.payload);
            }
            else{
                context.error = response.data.message;
            }
        }, (err) => {
            console.log(err)
            context.error = err
        });
    },
    getDisplayCollection(context){
        context.$http.get(DISPLAY_URL).then((response) => {
            if(response.data.status){
                //console.log(response.data.payload)
                //callback(response.data.payload);
                return response.data.payload;
            }
            else{
                return false;
            }
        }, (err) => {
            return false;
            console.log(err);
        });
    },
    heartbeatDisplayResponse( context, address, callback){
        context.$http.get('http://'+address+':9449/heartbeat').then((response) => {
            callback(true);
            //if(response.data.status){
            //    callback(response.data.payload);
            //}
            //else{
            //    context.error = response.data.message;
            //}
        }, (err) => {
            callback(false);
        });
    }
}