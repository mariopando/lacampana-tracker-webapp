/**
 * UI View Model
 * @class UI
 */
import Vue from 'vue'
import App from './components/App.vue'
import Home from './components/Home.vue'
import Login from './components/Login.vue'
import auth from './auth.js'
import Api from './api.js'

module.exports = function() {
    //++ Module
    var self        = this;
    self.moduleName = "ui";
    self.appIsUsed = false;
    self.router = null;
    self.displays = null;

    self.vm = {
        el: 'body',
        data: {
          currentView: 'Dashboard'
        },
        components: {
            //Login
        },
        events: {
            'setView': 'setView'
        },
        methods:{
            setView(view) {
                this.currentView = view;
                this.$broadcast('setView',view)
            },
            getDisplays(){
                var self = this;
                Api.getDisplays(this, function(response){
                    //add connected attribute to collection
                    var displays = _.map(response, function(element) {
                        return _.extend({}, element, { connected: false });
                    });
                    self.displays = displays;
                });
            }
        }
    };

    self.init = function(){
        self.loadRouting();
        self.loadScripts();
        core.modules.ui.vm.getDisplays();
    };

    self.loadRouting = function(){
        self.router = new VueRouter();
        self.router.map({
            '/home': {
                component: Home
            },
            '/login': {
                component: Login
            }
        });

        // Check the users auth status when the app starts
        auth.checkAuth();

        // Assign to headers page
        Vue.http.headers.common['Authorization'] = auth.getAuthHeader();

        // Redirect to the home route if any routes are unmatched
        self.router.redirect({
            '*': '/home'
        });

        self.router.start(App,'#wrapper');

        self.router.go('home');
    };

    self.loadGraphs = function(){
        var sparklineCharts = function(){
            $("#sparkline1").sparkline([34, 43, 43, 35, 44, 32, 44, 52], {
                type: 'line',
                width: '100%',
                height: '50',
                lineColor: '#1ab394',
                fillColor: "transparent"
            });

            $("#sparkline2").sparkline([32, 11, 25, 37, 41, 32, 34, 42], {
                type: 'line',
                width: '100%',
                height: '50',
                lineColor: '#1ab394',
                fillColor: "transparent"
            });

            $("#sparkline3").sparkline([34, 22, 24, 41, 10, 18, 16,8], {
                type: 'line',
                width: '100%',
                height: '50',
                lineColor: '#1C84C6',
                fillColor: "transparent"
            });
        };

        var sparkResize;

        $(window).resize(function(e) {
            clearTimeout(sparkResize);
            sparkResize = setTimeout(sparklineCharts, 500);
        });

        sparklineCharts();

    };

    self.loadScripts = function(){
        $.when(
            $.getScript( "js/plugins/flot/jquery.flot.js" ),
            $.getScript( "js/plugins/flot/jquery.flot.spline.js" ),
            $.getScript( "js/plugins/flot/curvedLines.js" ),
            $.getScript( "js/plugins/flot/jquery.flot.resize.js" ),
            $.getScript( "js/plugins/flot/jquery.flot.pie.js" ),
            $.getScript( "js/plugins/flot/jquery.flot.symbol.js" ),
            $.getScript( "js/plugins/flot/jquery.flot.time.js" ),
            $.getScript( "js/plugins/metisMenu/jquery.metisMenu.js" ),
            $.getScript( "js/plugins/slimscroll/jquery.slimscroll.min.js" ),
            //$.getScript( "js/plugins/dataTables/datatables.min.js" ),
            //$.getScript( "js/plugins/flot/jquery.flot.tooltip.min.js" ),
            $.getScript( "js/plugins/flot/jquery.flot.tooltip.js" ),
            $.getScript( "js/plugins/pace/pace.min.js" ),
            $.getScript( "js/plugins/sparkline/jquery.sparkline.min.js" ),
            //$.getScript( "js/plugins/footable/footable.all.min.js" ),
            $.getScript( "js/plugins/inspinia.js" ),
            $.Deferred(function( deferred ){
                $( deferred.resolve );
            })
        ).done(function(){
            self.loadGraphs();
            console.log('scripts loaded!')
        });
    };
};